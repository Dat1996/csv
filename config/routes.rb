Rails.application.routes.draw do
  get 'export_users/index'
  get 'home/index'
  root "home#index"
  resources :export_users
  resources :imports
end
